/**
  * @customElement
  * @polymer
  */
class LoginDialog extends Polymer.Element {
  static get is() { return 'login-dialog'; }
  static get properties() {
    return {
      username: {
        type: String,
        value:''
      },
      password: {
        type: String,
        value:''
      }, 
      passwordVisible: {
        type: Boolean,
        value: false
      }
    };
  }

  login(event, detail, target) {
    console.log('login presionado..')
    event.preventDefault();
  }

  reset(event, detail, target) {
    console.log('Cancelar presionado..');
    console.log(this.prop1 + " - " + this.username);
    event.preventDefault();
  }

  clearUserNameInput() {
    this.username = "";
  }

  clearUserPasswordInput() {
    this.password = "";
  }
  
  _getIcon(passwordVisible) {
    return passwordVisible ? 'icons:visibility' : 'icons:visibility-off';
  }

  _getType (passwordVisible) {
    return passwordVisible ? '' : 'password';
  }

  _togglePasswordVisible () {
    this.passwordVisible = !this.passwordVisible;
    this.getSha256();
  }

  clear() {
    this.set('value', '');
  }

  async generateSha256(message) {
    // encode as UTF-8
    const msgBuffer = new TextEncoder('utf-8').encode(message);
    // hash the message
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
    // convert ArrayBuffer to Array
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    // convert bytes to hex string
    const hashHex = hashArray.map(b => ('00' + b.toString(16)).slice(-2)).join('');
    return hashHex;
  }
  
  //sha256('abc').then(hash => console.log(hash));
  
  async getSha256() {
    const hash = await this.generateSha256(this.password);
    console.log("hash: " + hash);
  };
  


}



window.customElements.define(LoginDialog.is, LoginDialog);
