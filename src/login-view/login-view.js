{
  const CellsPage = customElements.get('cells-page');
  const { pagesMixin } = Gluon;

  class GluonLoginPage extends pagesMixin(CellsPage) {
    static get is() {
      return 'login-page';
    }

    static get properties() {
      return {};
    }

    _handleLoginRequest(event) {
      const { detail } = event;

      this._notifyLoadingStatus(true);
      this.$.loginDM.handleLogin(detail);
    }

    _handleLoginSuccess() {
      this._notifyLoadingStatus(false);
      this.navigate('pfm');
    }

    _handleLoginFail() {
      this._notifyLoadingStatus(false);
      this.$.modal.open();
    }
  }

  window.customElements.define(GluonLoginPage.is, GluonLoginPage);
}
